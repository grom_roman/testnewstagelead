<?php

namespace local\lib;

/*
 * Класс ф-й работы с лидами
 * */

class CTransitionNextStageWhenCreatingLead
{
        
    
    /*
    * Получение экземпляра класса хайлоад блока:
    * */

    public static function getEntityDataClass($hlBlockId)
    {
        if( empty($hlBlockId) || $hlBlockId<1){
            
        } else {
            if (\Bitrix\Main\Loader::includeModule('highloadblock') ) { 
                $hlblock= \Bitrix\Highloadblock\HighloadBlockTable::getById($hlBlockId)->fetch();
                $entity= \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                $entityDataClass=$entity->getDataClass();                
                return $entityDataClass;
            }
        }        
        return false;        
    }
    
    
    /*
    * Получение Массива всех значений хайлоад блока:
    * */
    public static function getHighloadProperty($hlBlockId)
    {
        $masParamsAllHighload=[];
         
        if (\Bitrix\Main\Loader::includeModule('highloadblock')) {     
            $hlBlockClass = self::getEntityDataClass($hlBlockId); 
            $rsData=$hlBlockClass::getList(array(
                'select'=>array('*')
            ));
            while($el=$rsData->fetch()){
                $masParamsAllHighload[$el['ID']]=$el;
                }             
        } 
        return $masParamsAllHighload;
    }

    


    /*
     * Перевод на следующую стадию Лида при его создании и совпадении с id из Хайлоад блока
     * */
    public function newStageLead($fields) 
    {
        $hlBlockId = 2; // Id хайлоад блока
        $newStageLead = 'IN_PROCESS'; // Стадия на которую нужно перевести лид
        $masParamsAllHighload = [];
        
        if($fields["ID"]) {
            
            //$masParamsAllHighload = $this->getHighloadPropertyR($hlBlockId); //Не работает
            $masParamsAllHighload = self::getHighloadProperty($hlBlockId);
            
            $flagNewStage = '';
            foreach($masParamsAllHighload as $idProp => $valProp) {          
                
                if($fields[$valProp['UF_LEAD_FIELD_CODE']] == $valProp['UF_LEAD_FIELD_VALUE']) {
                    $flagNewStage = 'yes';
                } else {
                    $flagNewStage = '';
                    break;        
                }
            }    
            
\Bitrix\Main\Diag\Debug::writeToFile($hlBlockId,"hlBlockId","local/var/logs/fields2.txt");
\Bitrix\Main\Diag\Debug::writeToFile($newStageLead,"newStageLead","local/var/logs/fields2.txt");
\Bitrix\Main\Diag\Debug::writeToFile($masParamsAllHighload,"masParamsAllHighload","local/var/logs/fields2.txt");
\Bitrix\Main\Diag\Debug::writeToFile($flagNewStage,"flagNewStage","local/var/logs/fields2.txt");

            
            if($flagNewStage == 'yes') {
                if (\Bitrix\Main\Loader::includeModule('crm')) { 
                            
                    $entity = new \CCrmLead(false);//true - проверять права на доступ
                    $new_fields = array( 
                        'STATUS_ID' => $newStageLead 
                    ); 
                    $entity->update($fields["ID"], $new_fields); 

\Bitrix\Main\Diag\Debug::writeToFile($fields["ID"],"Обновили лид","local/var/logs/fields2.txt");
                }
            }
 

\Bitrix\Main\Diag\Debug::writeToFile($fields,"все свойства","local/var/logs/fields2.txt");   
\Bitrix\Main\Diag\Debug::writeToFile("","==============================================================","local/var/logs/fields2.txt");         
        }
        
    }



}
?>