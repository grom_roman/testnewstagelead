<?php

// Автолоад необходимых классов
    \Bitrix\Main\Loader::registerAutoloadClasses(
        null,
        [
            '\local\lib\CTransitionNextStageWhenCreatingLead' => '/local/lib/CTransitionNextStageWhenCreatingLead.php',
        ]
    );

$eventManager = \Bitrix\Main\EventManager::getInstance();

//Перевод на следующую стадию Лида при его создании и совпадении с id из Хайлоад блока
    $eventManager->addEventHandler(
        'crm',
        'onAfterCrmLeadAdd',
        ['\local\lib\CTransitionNextStageWhenCreatingLead', 'newStageLead']
    );